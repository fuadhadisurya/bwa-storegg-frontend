import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import React from 'react'
import Footer from './Footer'
import MenuItem from './MenuItem'
import Profile from './Profile'

interface SidebarProps {
    activeMenu: 'overview' | 'transactions' | 'settings';
}

export default function SideBar(props: SidebarProps) {
    const { activeMenu } = props;
    const router = useRouter();
    const onLogout = () => {
        Cookies.remove('token');
        router.push('/sign-in');
    };
    return (
        <section className="sidebar">
            <div className="content pt-50 pb-30 ps-30">
                <Profile />
                <div className="menus">
                    <MenuItem title='Overview' icon='ic-menu-overview.svg' active={activeMenu==='overview'} href='/member' />
                    <MenuItem title='Transactions' icon='ic-menu-transactions.svg' active={activeMenu==='transactions'} href='/member/transactions' />
                    <MenuItem title='Messages' icon='ic-menu-messages.svg' href='/member' />
                    <MenuItem title='Card' icon='ic-menu-card.svg' href='/member' />
                    <MenuItem title='Rewards' icon='ic-menu-rewards.svg' href='/member' />
                    <MenuItem title='Settings' icon='ic-menu-settings.svg' active={activeMenu==='settings'} href='/member/edit-profile' />
                    <MenuItem title='Log Out' icon='ic-menu-logout.svg' onClick={onLogout} />
                </div>
                <Footer />
            </div>
        </section>
    )
}
