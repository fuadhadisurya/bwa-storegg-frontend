import cx from 'classnames'
import React from 'react'

interface ButtonTabProps {
    active: boolean;
    title: string;
    onClick: () => void;
}

export default function ButtonTab(props: ButtonTabProps) {
    const { active, title, onClick } = props;
    const btnClass = cx({
        'btn btn-status rounded-pill text-sm me-3': true,
        'btn-active': active
    })
    return (
        <button type='button' onClick={onClick} className={btnClass}>{title}</button>
    )
}
